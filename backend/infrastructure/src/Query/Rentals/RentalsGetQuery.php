<?php

declare(strict_types=1);

namespace App\Query\Rentals;

use App\Query\Query;

class RentalsGetQuery implements Query
{
    private $carId;
    private $date;

    public function __construct(array $params)
    {
        $this->carId = $params['carId'] ?? null;
        $this->date = $params['date'] ?? null;
    }

    public function getCarId(): ?string
    {
        return $this->carId;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }
}
