<?php

declare(strict_types=1);

namespace App\Query\Rentals;

use Doctrine\DBAL\Connection;

class RentalsGetQueryHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function handle(RentalsGetQuery $query): array
    {
        $sql = 'select * from public.rental where car_id = :carId';

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('carId', $query->getCarId());
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
