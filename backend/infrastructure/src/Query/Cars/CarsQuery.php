<?php

declare(strict_types=1);

namespace App\Query\Cars;

use App\Query\Query;

class CarsQuery implements Query
{
    private $make;
    private $model;
    private $segment;

    // params in array drawback: you have to check types manually
    public function __construct(array $params)
    {
        $this->make = $params['make'] ?? null;
        $this->model = $params['model'] ?? null;
        $this->segment = $params['segment'] ?? null;
    }

    public function getMake(): ?string
    {
        return $this->make;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function getSegment(): ?string
    {
        return $this->segment;
    }
}
