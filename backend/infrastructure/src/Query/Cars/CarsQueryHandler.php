<?php

declare(strict_types=1);

namespace App\Query\Cars;

class CarsQueryHandler
{
    public static function handle(CarsQuery $query): array
    {
        return [
            'fury' => 'oto fury marki '.$query->getMake().' model '.$query->getModel().' z segmentu '.$query->getSegment()
        ];
    }
}