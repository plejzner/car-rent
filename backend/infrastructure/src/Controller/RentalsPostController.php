<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\RentCar\RentCarCommand;
use App\Command\RentCar\RentCarCommandHandler;
use Symfony\Component\HttpFoundation\JsonResponse;

class RentalsPostController
{
    private $rentCarCommandHandler;

    public function __construct(RentCarCommandHandler $handler)
    {
        $this->rentCarCommandHandler = $handler;
    }

    public function __invoke(RentCarCommand $command): JsonResponse
    {
        $this->rentCarCommandHandler->handle($command);

        return new JsonResponse();
    }
}