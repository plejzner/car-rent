<?php

declare(strict_types=1);

namespace App\Controller;

use App\Query\Rentals\RentalsGetQuery;
use App\Query\Rentals\RentalsGetQueryHandler;
use Symfony\Component\HttpFoundation\JsonResponse;

class RentalsGetController
{
    private $handler;

    public function __construct(RentalsGetQueryHandler $handler)
    {
        $this->handler = $handler;
    }

    public function __invoke(RentalsGetQuery $query): JsonResponse
    {
        $result = $this->handler->handle($query);

        return new JsonResponse($result);
    }
}
