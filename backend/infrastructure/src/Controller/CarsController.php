<?php

namespace App\Controller;

use App\Query\Cars\CarsQuery;
use App\Query\Cars\CarsQueryHandler;
use Symfony\Component\HttpFoundation\JsonResponse;

class CarsController
{
    public function __invoke(CarsQuery $query): JsonResponse
    {
        $result = CarsQueryHandler::handle($query);

        return new JsonResponse($result);
    }
}