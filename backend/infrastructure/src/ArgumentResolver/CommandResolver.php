<?php

namespace App\ArgumentResolver;

use App\Command\Command;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class CommandResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        $className = $argument->getType();
        return new $className([]) instanceof Command;
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $className = $argument->getType();
        $params = $request->request->all();

        $command = new $className();
        foreach ($params as $paramName => $value) {
            $setter = 'set'.\ucfirst($paramName);
            if (\method_exists($command, $setter)) {
                $command->$setter($value);
            }
        }

        yield $command;
    }
}
