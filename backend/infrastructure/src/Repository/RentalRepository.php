<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Rental;
use Domain\RentalRepository as RentalRepositoryInterface;

class RentalRepository implements RentalRepositoryInterface
{
    private $entityManager;
    private $entityRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $entityManager->getRepository(Rental::class);
    }

    public function add(Rental $rental): void
    {
        $this->entityManager->persist($rental);
        $this->entityManager->flush();
    }

    public function get(string $rentalId): Rental
    {
        return $this->entityRepository->find($rentalId);
    }
}
