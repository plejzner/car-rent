<?php

declare(strict_types=1);

namespace App\Command\RentCar;

use App\Repository\RentalRepository;
use Domain\Rental;

class RentCarCommandHandler
{
    private $rentalRepository;

    public function __construct(RentalRepository $rentalRepository)
    {
        $this->rentalRepository = $rentalRepository;
    }

    public function handle(RentCarCommand $command): void
    {
        $this->rentalRepository->add(
            new Rental(
                'abc' . rand(0, 9999999),
                $command->getCarId(),
                $command->getCustomerId(),
                $command->getDateFrom(),
                $command->getDateTo()
            )
        );
    }
}
