<?php

declare(strict_types=1);

namespace App\Command\RentCar;

use App\Command\Command;

class RentCarCommand implements Command
{
    private $carId;
    private $customerId;
    private $dateFrom;
    private $dateTo;

    public function getCarId(): string
    {
        return $this->carId;
    }

    public function setCarId(string $carId): void
    {
        $this->carId = $carId;
    }

    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    public function setCustomerId(string $customerId): void
    {
        $this->customerId = $customerId;
    }

    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    public function setDateFrom(string $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    public function getDateTo(): string
    {
        return $this->dateTo;
    }

    public function setDateTo(string $dateTo): void
    {
        $this->dateTo = $dateTo;
    }
}