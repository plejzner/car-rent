<?php

declare(strict_types=1);

namespace App\Tests\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetCarsListTest extends WebTestCase
{
    public function test_endpoint_should_return_all_cars()
    {
        $client = static::createClient();

        $client->request('GET', '/cars');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // todo - assert expected content
    }

    public function test_endpoint_should_return_all_segment_C_Fords()
    {
        $client = static::createClient();

        $client->request('GET', '/cars?segment=C&make=Ford');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // todo - assert expected content
    }
}