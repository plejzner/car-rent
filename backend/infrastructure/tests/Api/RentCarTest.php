<?php

declare(strict_types=1);

namespace App\Tests\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RentCarTest extends WebTestCase
{
    // Should I name test after Domain action/command, or in terms of REST resources?
    // Let's go Domain way, and see what happens ;)
    public function test_rent_car_to_customer()
    {
        $client = static::createClient();

        $client->request('POST', '/rentals', [
            'carId' => 'car123',
            'customerId' => 'bobi456',
            'dateFrom' => (new \DateTimeImmutable('yesterday 12:00'))->format('Y-m-d H:i'),
            'dateTo' => (new \DateTimeImmutable('tomorrow 12:00'))->format('Y-m-d H:i'),
        ]);

        $client->request('GET', '/rentals', [
            'carId' => 'car123',
            'date' => (new \DateTimeImmutable('now'))->format('Y-m-d H:i')
        ]);

        $responseContent = \json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals('bobi456', $responseContent[0]['customer_id']);
    }
}
