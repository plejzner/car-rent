<?php

declare(strict_types=1);

namespace Domain;

/**
 * Aggregate root
 */
class Car
{
    private $carId;
    private $make;
    private $model;
    private $segment;

    public function __construct(string $carId, string $make, string $model, string $segment)
    {
        $this->carId = $carId;
        $this->make = $make;
        $this->model = $model;
        $this->segment = $segment;
    }

    public function getCarId(): string
    {
        return $this->carId;
    }

    public function getMake(): string
    {
        return $this->make;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function getSegment(): string
    {
        return $this->segment;
    }
}