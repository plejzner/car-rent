<?php

declare(strict_types=1);

namespace Domain;

/**
 * Aggregate root
 */
class Customer
{
    private $customerId;
    private $name;
    private $surname;
    private $pesel;

    public function __construct(string $customerId, string $name, string $surname, Pesel $pesel)
    {
        $this->customerId = $customerId;
        $this->name = $name;
        $this->surname = $surname;
        $this->pesel = $pesel;
    }

    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getPesel(): Pesel
    {
        return $this->pesel;
    }
}