<?php

declare(strict_types=1);

namespace Domain;

interface RentalRepository
{
    public function add(Rental $rental): void;

    public function get(string $rentalId): Rental;
}