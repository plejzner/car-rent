<?php

declare(strict_types=1);

namespace Domain;

/**
 * Value object
 */
class Pesel
{
    private $value;

    public function __construct(string $value)
    {
        if (\strlen($value) !== 11) {
            throw new \DomainException('Pesel must be 11 digits long');
        }

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}