<?php

declare(strict_types=1);

namespace Domain;

/**
 * Aggregate root
 */
class Rental
{
    private $rentalId;
    private $carId;
    private $customerId;
    private $dateFrom;
    private $dateTo;

    public function __construct(string $rentalId, string $carId, string $customerId, string $dateFrom, string $dateTo)
    {
        $this->carId = $carId;
        $this->customerId = $customerId;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->rentalId = $rentalId;
    }

    public function getRentalId(): string
    {
        return $this->rentalId;
    }

    public function getCarId(): string
    {
        return $this->carId;
    }

    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    public function getDateTo(): string
    {
        return $this->dateTo;
    }
}