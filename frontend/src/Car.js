import React, { Component } from 'react';
import './Car.css';

class Car extends Component {

  render() {

    return (
      <tr className="Car">
        <td>
          <span className="name">{this.props.data.make} {this.props.data.model}</span> <br/>
          <img src={process.env.PUBLIC_URL + '/img/' + this.props.data.photo}/>
        </td>
        <td>
          <span>{this.props.data.power}</span> <br/>
          <span>{this.props.data.transmission}</span> <br/>
          <span>{this.props.data.fuel}</span>
        </td>
        <td>
          <span>price:</span><br/>
          <p>krótkoterminowy: {this.props.data.price.shortTerm} zł/doba</p>
          <p>średnioterminowy: {this.props.data.price.midTerm} zł/doba</p>
          <p>długoterminowy: {this.props.data.price.longTerm} zł/doba</p>
        </td>
      </tr>
    );
  }
}

export default Car;
