import React, { Component } from 'react';
import Car from './Car.js';

class CarList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cars: [
        {
          make: 'Citroen',
          model: 'C3',
          photo: 'citroen-c3.png',
          power: '90 KM',
          transmission: 'manual',
          fuel: 'gasoline',
          price: {shortTerm: 99, midTerm: 88, longTerm: 77,}
        },
        {
          make: 'Hyundai',
          model: 'i30',
          photo: 'hyundai-i30.png',
          power: '105 KM',
          transmission: 'automatic',
          fuel: 'oil',
          price: {shortTerm: 120, midTerm: 100, longTerm: 90,}
        },
      ]
    }
  }
  render() {

    const cars = this.state.cars.map(
      (carData) => {
        return <Car
          data={carData}
        />;
      }
    );

    return (
      <table className="CarList">
        {cars}
      </table>
    );
  }
}

export default CarList;