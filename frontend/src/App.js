import React, { Component } from 'react';
import './App.css';
import CarList from './CarList.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CarList/>
      </div>
    );
  }
}

export default App;
